#pragma once

#include "iship.h"
#include "ship_parts.h"
#include <vector>
#include <memory>
#include <SDL.h>

struct ShipPart;
class Texture;
class Camera;

class Ship : public IShip
{
public:
  /// Default constructor
  Ship(const std::string& name);

  /// Returns the name of the ship
  const char* name() const { return name_.c_str(); }

  /// Returns the mass of the ship
  float mass() const override { return configuration_.mass; }

  /// Returns the mass moment of inertia
  float moment_of_inertia() const override { return momentOfInertia_; }

  /// Returns the position of the ship
  Float2 position() const override { return position_; }

  /// Returns the current angular velocity of the ship
  float angular_velocity() const override { return angularVelocity_; }

  /// Returns the linear velocity of the ship
  Float2 linear_velocity() const override { return linearVelocity_; }

  /// Returns the rotation of the ship
  float rotation() const override { return rotation_; }

  /// Returns the number of thrusters the ship has.
  uint32_t num_thrusters() const override { return configuration_.numThrusters; }

  /// Returns information on a specific thruster on the ship.
  ShipThrusterDesc thruster(int idx) const override;

  /// Returns a value describing how much power is going to a thruster.
  float thruster_power(int thrusterIdx) const override { return thrusterPowers_[thrusterIdx]; }

  /// Sets the amount of power going to a thruster
  void set_thruster_power(int thrusterIdx, float power) override;
  
  /// Sets the position of the ship
  void set_position(const Float2& position);

  /// Sets the rotation of the ship
  void set_rotation(float radians);
  
  /// Called to render the ship
  void Draw(SDL_Renderer* renderer, const Camera& camera, const Texture &shipsTexture);

  /// Update the ship
  void Update(float dt);

  /// Applies a force on the ship at the given position
  void ApplyForce(float fx, float fy, float F, float x, float y);

private:
  /// Draws a specific part of the ship
  void DrawPart(SDL_Renderer* renderer, const Camera& camera, const Texture &shipTexture, ShipPart const* part, 
    const SDL_Point& partPosition, const SDL_Point& textureOrigin, float rotation, float scale, bool flipHorizontally = false);

  /// Adds the force of the trusters to the ship
  void AddThrusterForce(float dt);

private:
  std::string name_;

  Float2 position_;
  float rotation_;
  float rotationSin_, rotationCos_;

  Float2 linearVelocity_;
  float angularVelocity_;

  float momentOfInertia_;

  std::vector<float> thrusterPowers_;

  const ShipConfiguration &configuration_;
};