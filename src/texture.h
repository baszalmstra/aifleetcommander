#pragma once

#include <string>
#include <cstdint>

struct SDL_Texture;
struct SDL_Renderer;

class Texture
{
public:
  Texture();
  ~Texture();

  /// Loads the texture from the given file
  bool Load(SDL_Renderer* renderer, const std::string &filename);

  /// Returns the width of the texture
  uint16_t width() const { return height_; }

  /// Returns the height of the textyre
  uint16_t height() const { return width_; }

  /// Returns a pointer to the SDL texture object
  SDL_Texture *texture() const { return texture_; }

  /// Returns true if the texture is valid
  bool is_valid() const { return texture_ != nullptr; }

private:
  SDL_Texture *texture_;

  uint16_t width_, height_;
};