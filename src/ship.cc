#include "ship.h"
#include "texture.h"
#include "camera.h"
#include "ship_parts.h"
#include <cmath>

//---------------------------------------------------------------------------------------------------
Ship::Ship(const std::string &name) :
  name_(name),
  position_(0.0f, 0.0f),
  rotation_(0.0f),
  linearVelocity_(0.0f, 0.0f),
  angularVelocity_(0.0f),
  configuration_(shipConfigurations[rand() % shipConfigurationsCount])
{
  thrusterPowers_.resize(configuration_.numThrusters, 0);
  momentOfInertia_ = configuration_.mass * 40 * 40 / 2.0f;
  rotationSin_ = std::sin(rotation_);
  rotationCos_ = std::cos(rotation_);
}

//---------------------------------------------------------------------------------------------------
void Ship::set_position(const Float2& position)
{
  position_ = position;
}

//---------------------------------------------------------------------------------------------------
void Ship::set_rotation(float rotation)
{
  rotation_ = rotation;
}

//---------------------------------------------------------------------------------------------------
void Ship::Draw(SDL_Renderer* renderer, const Camera& camera, const Texture &shipTexture)
{
  static ShipPart const *truster = GetPartByName("fire05");

  // Render all thrusters
  for (int i = 0; i < configuration_.numThrusters; ++i)
  {
    // Give them a somewhat random opacity because it looks cool
    const ThrusterPart &thruster = configuration_.thrusters[i];
    uint8_t col = static_cast<uint8_t>(rand() % 100 + 155);
    SDL_SetTextureAlphaMod(shipTexture.texture(), col);

    // Draw them with a size based on their thrust but with a rather steap (power of 4) curve so we can see it easily
    DrawPart(renderer, camera, shipTexture, truster, { static_cast<int>(thruster.x), static_cast<int>(thruster.y) }, 
      { truster->width / 2, truster->height }, 
      -thruster.rotation, 1.0f - std::pow(1.0f - thrusterPowers_[i], 4), 
      false);
  }
  
  // Reset alpha 
  SDL_SetTextureAlphaMod(shipTexture.texture(), 255);

  // Draw all ship parts
  for (int i = 0; i < configuration_.numParts; ++i)
  {
    const ShipPartPlacement &part = configuration_.parts[i];
    DrawPart(renderer, camera, shipTexture, part.part, { part.x, part.y }, { part.originX, part.originY }, 0.0f, 1.0f, part.flip);
  }
}

//---------------------------------------------------------------------------------------------------
void Ship::DrawPart(SDL_Renderer* renderer, const Camera& camera, const Texture &shipTexture, ShipPart const* part,
                    const SDL_Point& partPosition, const SDL_Point& textureOrigin, float rotation, float scale, bool flipHorizontally)
{
  // Incorporate rotation of the vehicle
  float partRotation = (rotation + rotation_) * (180.0f / 3.141592654f);
 
  float partPosX = partPosition.x*rotationCos_ - partPosition.y*rotationSin_ + position_.x - camera.x - (textureOrigin.x * scale);
  float partPosY = partPosition.x*rotationSin_ + partPosition.y*rotationCos_ + position_.y - camera.y - (textureOrigin.y * scale);

  SDL_Point origin = { static_cast<int>(textureOrigin.x * scale), static_cast<int>(textureOrigin.y * scale) };
  SDL_Rect partRect = { part->x, part->y, part->width, part->height };
  SDL_Rect partDstRect = { static_cast<int>(partPosX), static_cast<int>(partPosY), static_cast<int>(part->width * scale), static_cast<int>(part->height * scale)};

  SDL_RendererFlip flip = SDL_FLIP_NONE;
  if (flipHorizontally)
  {
    flip = SDL_FLIP_HORIZONTAL;
    partDstRect.x -= partDstRect.w;
    origin.x = partDstRect.w - origin.x;
  }

  SDL_RenderCopyEx(renderer, shipTexture.texture(), &partRect, &partDstRect, partRotation, &origin, flip);
}

//---------------------------------------------------------------------------------------------------
void Ship::Update(float dt)
{
  //thrusterPowers_[2] =
  //thrusterPowers_[3] = 0.9f;
  thrusterPowers_[1] =
  thrusterPowers_[0] = 0.9f;

  AddThrusterForce(dt);

  position_.x += linearVelocity_.x * dt;
  position_.y += linearVelocity_.y * dt;
  rotation_ += angularVelocity_ * dt;
  rotation_ = fmod(rotation_, 3.141592654f*2.0f);
  if (rotation_ < 0)
    rotation_ += 3.141592654f*2.0f;
    
  rotationSin_ = std::sin(rotation_);
  rotationCos_ = std::cos(rotation_);
}

//---------------------------------------------------------------------------------------------------
void Ship::AddThrusterForce(float dt)
{
  float thrusterAccelX = 0.0f;
  float thrusterAccelY = 0.0f;
  float thrusterTorque = 0.0f;

  for (int i = 0; i < configuration_.numThrusters; ++i)
  {
    const ThrusterPart& thruster = configuration_.thrusters[i];
    float force = thruster.maxForce*thrusterPowers_[i];;
 
    // Calculate the direction of the truster under the current rotation of the ship
    float tx = std::sin(thruster.rotation - rotation_);
    float ty = std::cos(thruster.rotation - rotation_);

    // Calculate the current position of the truster in world space
    float px = thruster.x*rotationCos_ - thruster.y*rotationSin_;
    float py = thruster.x*rotationSin_ + thruster.y*rotationCos_;

    // Add it to the linear velocity
    thrusterAccelX += tx*force / mass();
    thrusterAccelY += ty*force / mass();

    // Add it to the angular velocity
    thrusterTorque += (px * ty - py * tx)*force / momentOfInertia_;
  }

  linearVelocity_.x += thrusterAccelX*dt;
  linearVelocity_.y += thrusterAccelY*dt;
  angularVelocity_ += thrusterTorque*dt;
}

//---------------------------------------------------------------------------------------------------
ShipThrusterDesc Ship::thruster(int idx) const
{
  if (idx < 0 || idx >= configuration_.numThrusters)
    throw std::invalid_argument("Invalid thruster index");

  ShipThrusterDesc desc;
  desc.maxThrust = configuration_.thrusters[idx].maxForce;
  desc.position = Float2(configuration_.thrusters[idx].x, configuration_.thrusters[idx].y);
  desc.orientation = configuration_.thrusters[idx].rotation;
  return desc;
}

//---------------------------------------------------------------------------------------------------
void Ship::set_thruster_power(int idx, float power)
{
  if (idx < 0 || idx >= configuration_.numThrusters)
    throw std::invalid_argument("Invalid thruster index");

  thrusterPowers_[idx] = power;
}