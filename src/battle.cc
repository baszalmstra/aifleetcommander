#include "battle.h"
#include "ship.h"

//---------------------------------------------------------------------------------------------------
Battle::Battle()
{
  for (uint32_t i = 0; i < 100; ++i)
  {
    Ship *ship = new Ship("ship");
    ship->set_position(Float2(static_cast<float>(rand() % 5000), static_cast<float>(rand() % 5000)));
    ship->set_rotation(rand() / (float) RAND_MAX * 3.141592654f);
    ships_.emplace_back(ship);
  }
}

//---------------------------------------------------------------------------------------------------
Battle::~Battle()
{

}

//---------------------------------------------------------------------------------------------------
bool Battle::Init(SDL_Renderer *renderer)
{
  // Load the ship textures
  if (!shipTextures.Load(renderer, "ships.png"))
    return false;

  return true;
}

//---------------------------------------------------------------------------------------------------
void Battle::Update(float dt)
{
  for (auto &ship : ships_)
    ship->Update(dt);
}

//---------------------------------------------------------------------------------------------------
void Battle::Draw(SDL_Renderer* renderer, const Camera& camera)
{
  for (auto &ship : ships_)
    ship->Draw(renderer, camera, shipTextures);  
}