#pragma once

#include "texture.h"
#include <memory>
#include <vector>

struct SDL_Renderer;
class Ship;
class Camera;

class Battle
{
public:
  // Default constructor
  Battle();

  // Default destructor
  ~Battle();

  // Initializes the battle
  bool Init(SDL_Renderer *renderer);

  // Called to draw the battle to the screen
  void Draw(SDL_Renderer* renderer, const Camera& camera);

  // Called to update the battle
  void Update(float dt);
  
private:
  Texture shipTextures;

  std::vector<std::unique_ptr<Ship>> ships_;
};