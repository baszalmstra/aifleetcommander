#pragma once

#include <cstdint>

//---------------------------------------------------------------------------------------------------
struct ShipPart
{
  const char* name;
  int x, y, width, height;
};

extern const ShipPart shipParts [];
extern const uint32_t shipPartsCount;

ShipPart const* GetPartByName(const char* name);

//---------------------------------------------------------------------------------------------------
struct ThrusterPart
{
  float x, y;
  float rotation;
  float maxForce;
};

//---------------------------------------------------------------------------------------------------
struct ShipPartPlacement
{
  ShipPart const* part;
  int x, y;
  int originX, originY;
  bool flip;
};

//---------------------------------------------------------------------------------------------------
struct ShipConfiguration
{
  float mass;
  int numParts;
  int numThrusters;
  ShipPartPlacement const* parts;  
  ThrusterPart const* thrusters;
};

extern const uint32_t shipConfigurationsCount;
extern const ShipConfiguration shipConfigurations [];