#include "application.h"
#include <iostream>
#include <SDL.h>
#include <SDL_video.h>
#include "battle.h"

//---------------------------------------------------------------------------------------------------
Application::Application() : 
  window_(nullptr),
  renderer_(nullptr),
  shouldQuit_(false)
{

}

//---------------------------------------------------------------------------------------------------
Application::~Application()
{

}

//---------------------------------------------------------------------------------------------------
int Application::Run()
{
  // Initialize SDL
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_SYSTEM, "Unable to initialize SDL: %s", SDL_GetError());
    return -1;
  }

  // Initialize video
  if (!InitializeVideo())
  {
    SDL_Quit();
    return -1;
  }

  // Create the battle
  battle_.reset(new Battle());
  if (!battle_->Init(renderer_))
    return -1;
  
  // Run the main loop
  int result = Loop() ? 0 : -1;
    
  // Tear down graphics
  DestroyVideo();

  // Shutdown
  SDL_Quit();
  return result;
}

//---------------------------------------------------------------------------------------------------
bool Application::InitializeVideo()
{
  // Determine current display mode so we can setup proper fullscreen window
  SDL_DisplayMode displayMode;
  SDL_GetCurrentDisplayMode(0, &displayMode);

  // Create a window
  window_ = SDL_CreateWindow("AiFleetCommander", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, displayMode.w, displayMode.h,
    SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS);
  if (window_ == nullptr)
  {
    SDL_LogError(SDL_LOG_CATEGORY_SYSTEM, "Unable to create a window: %s", SDL_GetError());
    return false;
  }

  // Create a renderer
  renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);
  if (renderer_ == nullptr)
  {
    SDL_DestroyWindow(window_);
    window_ = nullptr;

    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Unable to create a renderer: %s", SDL_GetError());
    return false;
  }

  // Setup some hints
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

  return true;
}

//---------------------------------------------------------------------------------------------------
void Application::DestroyVideo()
{
  // Destroy the renderer
  if (renderer_ != nullptr)
  {
    SDL_DestroyRenderer(renderer_);
    renderer_ = nullptr;
  }

  // Destroy the window
  if (window_ != nullptr)
  {
    SDL_DestroyWindow(window_);
    window_ = nullptr;
  }
}

//---------------------------------------------------------------------------------------------------
bool Application::Loop()
{
  lastTime_ = SDL_GetTicks();

  SDL_Event evt;
  do
  {
    // Poll events
    while (SDL_PollEvent(&evt))
      HandleEvent(evt);

    // Update the game
    Update();

    // Render
    Render();

  } while (!shouldQuit_);

  return true;
}

//---------------------------------------------------------------------------------------------------
void Application::Update()
{
  const uint32_t updateSpeed = 15;

  uint32_t now = SDL_GetTicks();
  
  // Don't let the difference become more than 1 second
  if (now - lastTime_ > 1000)
    lastTime_ = now - 1000;
  
  // Try to update the battle with at least 15ms per frame
  int stsp = 0;
  while (lastTime_ + updateSpeed < now)
  {
    battle_->Update(updateSpeed * 1e-3f);
    lastTime_ += updateSpeed;
    stsp++;
  } 
}

//---------------------------------------------------------------------------------------------------
void Application::Render()
{
  SDL_SetRenderDrawColor(renderer_, 9, 10, 12, 255);
  SDL_RenderClear(renderer_);
  
  SDL_Rect vp;
  SDL_RenderGetViewport(renderer_, &vp);

  float scale;
  if (vp.w > vp.h)
    scale = 5000.0f / vp.w;
  else
    scale = 5000.0f / vp.h;

  SDL_RenderSetLogicalSize(renderer_, vp.w*scale, vp.h*scale);
  camera_.x = 0.0f;
  camera_.y = 0.0f;

  // Draw the battle
  if (battle_) battle_->Draw(renderer_, camera_);
  
  SDL_RenderPresent(renderer_);
}

//---------------------------------------------------------------------------------------------------
void Application::HandleEvent(const SDL_Event& evt)
{
  // Handle quit 
  if (evt.type == SDL_QUIT || evt.type == SDL_WINDOWEVENT_CLOSE)
    shouldQuit_ = true;

  // Handle alt+F4
  if (evt.type == SDL_KEYDOWN && evt.key.keysym.sym == SDLK_F4 &&
    (evt.key.keysym.mod == KMOD_LALT || evt.key.keysym.mod == KMOD_RALT))
    shouldQuit_ = true;
}
