#pragma once

#include "camera.h"
#include <memory>

struct SDL_Window;
struct SDL_Renderer;
union SDL_Event;

class Battle;

class Application
{
public:
  // Default constructor
  Application();

  // Default destructor
  ~Application();

  // Never copy this sucker
  Application(const Application&) = delete;
  Application(Application&&) = delete;

public:
  // This method should be called from the entry point of the application to kickstart the application
  int Run();

protected:
  // Handles sdl events
  void HandleEvent(const SDL_Event& evt);

  // Called once per frame to update the application
  void Update();

  // Called once per frame to render the application
  void Render();

private:
  // Initializes the video stuff
  bool InitializeVideo();

  // Destroy the video stuff
  void DestroyVideo();

  // Runs the main loop
  bool Loop();

private:
  SDL_Window *window_;
  SDL_Renderer *renderer_;

  bool shouldQuit_;
  uint32_t lastTime_;

  std::unique_ptr<Battle> battle_;
  Camera camera_;
};