#include "texture.h"
#include <SDL_image.h>

//---------------------------------------------------------------------------------------------------
Texture::Texture() : texture_(nullptr)
{

}

//---------------------------------------------------------------------------------------------------
Texture::~Texture()
{
  if (texture_ != nullptr)
  {
    SDL_DestroyTexture(texture_);
    texture_ = nullptr;
  }
}

//---------------------------------------------------------------------------------------------------
bool Texture::Load(SDL_Renderer* renderer, const std::string &path)
{
  // Try to load the image into memory
  SDL_Surface *surface = IMG_Load(path.c_str());
  if (surface == nullptr)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Unable to load %s: %s", path.c_str(), IMG_GetError());
    SDL_SetError(IMG_GetError());
    return false;
  }

  // Store dimensions
  width_ = static_cast<uint16_t>(surface->w);
  height_ = static_cast<uint16_t>(surface->h);

  // Create a texture out of it
  texture_ = SDL_CreateTextureFromSurface(renderer, surface);
  
  // Destroy the surface
  SDL_FreeSurface(surface);
  
  return texture_ != nullptr;
}