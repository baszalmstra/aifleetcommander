#pragma once

#include "types.h"

struct ShipThrusterDesc
{
  Float2 position;
  float orientation;
  float maxThrust;
};

struct IShip
{
public:
  /**
   * @brief Returns the name of the ship
   * @remarks Ships can be named so that they are easily identified. Ship names can be toggled on 
   *  and off in the simulation.
   */
  virtual const char* name() const = 0;

  /**
   * @brief Returns the mass of the ship.
   * @remarks The mass of the ship is constant during the simulation and is totally dependand on
   *  the configuration of the ship.
   */
  virtual float mass() const = 0;

  /**
   * @brief Returns the mass moment of inertia of the ship.
   * @remarks The mass (and moment of inertia) of the ship are constant during the simulation and
   *  are determined by the configuration of the ship.
   */
  virtual float moment_of_inertia() const = 0;

  /**
   * @brief Returns the number of thrusters the ship has.
   * @remarks Thrusters can be used to move the ship. However, since they are positioned at specific
   *  places on the hull of the ship each one has a very different thrust vector.
   * @see thruster
   */
  virtual uint32_t num_thrusters() const = 0;

  /**
   * @brief Returns information on a specific thruster on the ship.
   * @remarks Thrusters can be used to move the ship. However, since they are positioned at specific
   *  places on the hull of the ship each one has a very different thrust vector.
   * @see num_thrusters
   */
  virtual ShipThrusterDesc thruster(int idx) const = 0;

  /**
   * @brief Returns a value describing how much power is going to a thruster.
   * @details 0 means no power is going to the thruster and so it's not firing. 1 means the thruster is
   *  at full power.
   * @see set_thruster_power
   */
  virtual float thruster_power(int thrusterIdx) const = 0;

  /**
  * @brief Set the amount of power a thruster uses.
  * @details 0 means no power is going to the thruster and so it's not firing. 1 means the thruster is
  *  at full power.
  * @see thruster_power
  */
  virtual void set_thruster_power(int thrusterIdx, float power) = 0;

public:
  /**
   * @brief Returns the current position of the ship.
   */
  virtual Float2 position() const = 0;
    
  /**
   * @brief Returns the current orientation of the ship.
   */
  virtual float rotation() const = 0;

  /**
   * @brief Returns the angular velocity of the ship
   */
  virtual float angular_velocity() const = 0;

  /**
   * @brief Returns the linear velocity of the ship.
   */
  virtual Float2 linear_velocity() const = 0;
};