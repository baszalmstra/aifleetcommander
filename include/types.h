#pragma once

#include <cstdint>

struct Float2
{
  Float2() {};
  Float2(float _x, float _y) : x(_x), y(_y) {};

  float x;
  float y;
};